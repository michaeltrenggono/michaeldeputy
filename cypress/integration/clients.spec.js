describe("Client List Page", () => {
    it("should show client page", () => {
        cy.visit("http://localhost:8080/");
    });

    it("should only have 1 h1 in the page", () => {
        cy.get("h1").should("have.length", 1);
    });

    it("should have only 1 header", () => {
        cy.get(".header").should('have.length', 1);
    });

    it("should have filters", () => {
        cy.get(".filterDropdown").should("be.visible");
    });

    it("should have clear button", () => {
        cy.get(".filterClear").should("be.visible");
    });

    it("should have client cards", () => {
        cy.get(".clientCard").should("be.visible");
    });

    it("should have paginations", () => {
        cy.get(".pagination").should("be.visible");
    });
});
