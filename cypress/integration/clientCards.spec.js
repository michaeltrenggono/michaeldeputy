describe("client card", () => {
    it("should visit client page", () => {
        cy.visit("http://localhost:8080/");
    });

    it("should have logo", () => {
        cy.get('.clientCard .imageContainer img').should('have.attr', 'src').should('not.be.empty')
    });

    it("should have description", () => {
        cy.get('.clientCard .card-text').should('not.be.empty')
    });

    it("should have read more link", () => {
        cy.get('.clientCard .cta a').should('have.attr', 'href').should('not.be.empty')
    });

    it("should have min read time", () => {
        cy.get('.clientCard .cta .readTime').should('not.be.empty')
    });
});
