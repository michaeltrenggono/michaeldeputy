describe("pagination in client page", () => {
    it("should visit client page", () => {
        cy.visit("http://localhost:8080/");
    });

    it("should start at page 1", () => {
        cy.get('.paginationNav .pagination .active').should('have.text', 1);
    });

    it("should use parameter p to navigate page", () => {
        cy.visit("http://localhost:8080/?p=2");
        cy.get('.paginationNav .pagination .active').should('have.text', 2);
    });

    it("should use navigate page when a page number is clicked", () => {
        cy.get('.paginationNav .pagination').first().click();
        cy.url().should('include', 'p=1');
    });
});
