describe("filters in client page", () => {
    it("should visit client page", () => {
        cy.visit("http://localhost:8080/");
    });

    it("should be empty at first", () => {
        cy.get('.clientFilters .dropdownInput button span').first().should('not.have.text');
    });

    it("should show options on click", () => {
        cy.get('.clientFilters .dropdownInput').first().click().then(() => {
            cy.get('.clientFilters .dropdownInput .dropdown-menu').first().should('have.length', 1);
        });
    });

    it("should select option on click", () => {
        cy.get('.clientFilters .dropdownInput .dropdown-menu .option').eq(1).click().then(() => {
            cy.get('.clientFilters .dropdownInput button span').first().should('not.be.empty');
        });
    });

    it("should remove option if Any is selected", () => {
        cy.get('.clientFilters .dropdownInput').first().click().then(() => {
            cy.get('.clientFilters .dropdownInput .dropdown-menu').first().scrollTo('top');

            cy.get('.clientFilters .dropdownInput .dropdown-menu .option').first().click().then(() => {
                cy.get('.clientFilters .dropdownInput button span').first().should('not.have.text');
            });
        });
    });

    it("should clear option if clear is clicked", () => {
        cy.get('.clientFilters .filterClear').click().then(() => {
            cy.get('.clientFilters .dropdownInput button span').first().should('not.have.text');
        });
    });
});
