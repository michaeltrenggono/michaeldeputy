import { createApp } from 'vue';
import App from './App.vue';

const app = createApp(App);

app.config.globalProperties.$filters = {
    trimString(text, limit) {
        if (text.length > limit) {
            let trimmedString = text.substring(0, limit);
            trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")));
            return trimmedString.concat("...");
        }

        return text;
    },
    readTimeInMin(wordCount, wordPerMinute = 200) {
        return Math.floor(wordCount/wordPerMinute);
    }
};

app.mount('#app');
