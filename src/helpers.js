export const getCurrentTime = () => {
    const today = new Date();
    return today.getTime();
};
