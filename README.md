# clients

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Tests
```
(run npm run serve first, make sure it is running in separate terminal)
(open a new terminal and run below command)
npm run cy:open -> in cypress window, run (x) integration specs
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
